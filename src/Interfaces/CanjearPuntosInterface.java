package Interfaces;
import TDA.*;
import Entidades.*;


public interface CanjearPuntosInterface {
 
        /**
         *      	
         * @param catalogo es dato de entrada
         * @param maximosRegalosResultado es parte del resultado a devolver
         * @param puntosAcumulados es dato de entrada
         * @return devuelve la m�xima cantidad de puntos gastados
         */
		public int maximizarRegalosARetirar(ConjuntoTDA<Regalo> catalogo, ConjuntoTDA<Regalo> maximosRegalosResultado,int puntosAcumulados);
		/**
		 * 
		 * @param regalosARetirar es dato de entrada
		 * @return devuelve el tablero obtenido
		 */
		public MatrizTDA<Regalo> obtenerTableroMultirubroBalanceado(ConjuntoTDA<Regalo> regalosARetirar); 
}
