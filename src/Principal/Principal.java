package Principal;

import Interfaces.*;
import Implementacion.*;
import TDA.*;
import Implementaciones.*;
import Entidades.*;

public class Principal {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CanjearPuntosInterface canjearPuntos = new CanjearPuntosInterfaceImp2();
		
		ConjuntoTDA<Regalo> catalogo = new Conjunto<Regalo>();
		catalogo.inicializarConjunto();
		Principal.inicializarCatalogo(catalogo);
		//cambiar valores para las pruebas
		int ptosAcumulados = 60;
		ConjuntoTDA<Regalo> maximosRegalos = new Conjunto<Regalo>();
		maximosRegalos.inicializarConjunto();
		
		int puntosUtilizados = canjearPuntos.maximizarRegalosARetirar(catalogo, maximosRegalos, ptosAcumulados);
		
		Principal.mostrarProductos(maximosRegalos, puntosUtilizados);
	
		Principal.inicializarCatalogo(catalogo);

		MatrizTDA<Regalo> tableroPolirubro = canjearPuntos.obtenerTableroMultirubroBalanceado(catalogo);
	    
		Principal.mostrarTablero(tableroPolirubro);

	}

	
	private static void inicializarCatalogo( ConjuntoTDA<Regalo> c){
//		Regalo r1=new Regalo();
//		r1.setearNombre("Regalo1");
//		r1.setearPrecio(1);
//		r1.setearPuntos(30);
//		r1.setearRubro(1);
//		c.agregar(r1);
//		Regalo r2=new Regalo();
//		r2.setearNombre("Regalo2");
//		r2.setearPrecio(3);
//		r2.setearPuntos(20);
//		r2.setearRubro(2);
//		c.agregar(r2);
//		Regalo r3=new Regalo();
//		r3.setearNombre("Regalo3");
//		r3.setearPrecio(5);
//		r3.setearPuntos(5);
//		r3.setearRubro(3);
//		c.agregar(r3);
//		Regalo r4=new Regalo();
//		r4.setearNombre("Regalo4");
//		r4.setearPrecio(8);
//		r4.setearPuntos(15);
//		r4.setearRubro(4);
//		c.agregar(r4);
//		Regalo r5=new Regalo();
//		r5.setearNombre("Regalo5");
//		r5.setearPrecio(11);
//		r5.setearPuntos(15);
//		r5.setearRubro(5);
//		c.agregar(r5);
		
		Regalo r1=new Regalo();
		r1.setearNombre("Regalo1");
		r1.setearPrecio(2);
		r1.setearPuntos(30);
		r1.setearRubro(1);
		c.agregar(r1);
		Regalo r2=new Regalo();
		r2.setearNombre("Regalo2");
		r2.setearPrecio(2);
		r2.setearPuntos(20);
		r2.setearRubro(2);
		c.agregar(r2);
		Regalo r3=new Regalo();
		r3.setearNombre("Regalo3");
		r3.setearPrecio(2);
		r3.setearPuntos(5);
		r3.setearRubro(3);
		c.agregar(r3);
		Regalo r4=new Regalo();
		r4.setearNombre("Regalo4");
		r4.setearPrecio(9);
		r4.setearPuntos(15);
		r4.setearRubro(4);
		c.agregar(r4);
		Regalo r5=new Regalo();
		r5.setearNombre("Regalo5");
		r5.setearPrecio(2);
		r5.setearPuntos(15);
		r5.setearRubro(5);
		c.agregar(r5);
	}
	

    private static void mostrarProductos(ConjuntoTDA<Regalo> regalos, int puntosUtilizados){
    	System.out.println("La cantidad de puntos utilizados es: "+ puntosUtilizados);
    	
    	ConjuntoTDA<Regalo> auxiliar = new Conjunto<Regalo>();
    	auxiliar.inicializarConjunto();
    	
    	Regalo r;
    	
    	System.out.println("Los elementos que maximizan el resultado son: ");
    	while (!regalos.conjuntoVacio()){
    		r = regalos.elegir();
    		regalos.sacar(r);
    		auxiliar.agregar(r);
    		System.out.println("Elemento: "+r.toString());
    	}
    
    	while (!auxiliar.conjuntoVacio()){
    		r = auxiliar.elegir();
    		auxiliar.sacar(r);
    		regalos.agregar(r);
    	}
    }
    
    private static void mostrarTablero(MatrizTDA<Regalo> t){
    	
    	for (int i = 0; i < t.obtenerDimension() ; i++){
    	    for (int j = 0; j < t.obtenerDimension(); j++){
    	    	System.out.println("Coordenada x: "+ i+" Coordenada y: "+j+" : "+t.obtenerValor(i, j).toString());
    	    }	
    	}   	
    }
}
