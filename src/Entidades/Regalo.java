package Entidades;

public class Regalo {
	String nombre;
	int precio;
	int puntos;
	int nroRubro;
	
	public void setearNombre(String n){
		this.nombre = n;
	}
	
	public String obtenerNombre(){
		return this.nombre;
	}
	
	public void setearPrecio(int p){
		this.precio = p;
	}
	
	public int obtenerPrecio(){
		return this.precio;
	}
	
	public void setearPuntos(int ptos){
		this.puntos = ptos;
	}
	
	public int obtenerPuntos(){
		return this.puntos;
	}
	
	public void setearRubro(int r){
		this.nroRubro = r;
	}
	
	public int obtenerNroRubro(){
		return this.nroRubro;
	}
	
	@Override
	public String toString(){
       return new String("Nombre:"+this.nombre+" Precio:"+this.precio+" Puntos:"+this.puntos+" Nro. Rubro:"+this.nroRubro);
    }
	
	@Override 
	public boolean equals(Object r){ 
	          
	   return r.getClass().equals(this.getClass()) && (this.obtenerNombre() == ((Regalo)r).obtenerNombre()
	          && this.obtenerNroRubro()==((Regalo)r).obtenerNroRubro() && this.obtenerPrecio()==((Regalo)r).obtenerPrecio()
	          && this.obtenerPuntos()==((Regalo)r).obtenerPuntos()); 
	 
	 }
	
}
