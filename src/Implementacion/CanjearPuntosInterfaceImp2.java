package Implementacion;

import Entidades.Regalo;
import Implementaciones.ColaPrioridad;
import Implementaciones.Conjunto;
import Implementaciones.Matriz;
import Implementaciones.Vector;
import Interfaces.CanjearPuntosInterface;
import TDA.ConjuntoTDA;
import TDA.MatrizTDA;
import TDA.VectorTDA;

public class CanjearPuntosInterfaceImp2 implements CanjearPuntosInterface {

	private int maxDim;
	private int cantidadRegalos;

	@Override
	public int maximizarRegalosARetirar(ConjuntoTDA<Regalo> catalogo, ConjuntoTDA<Regalo> maximosRegalosResultado, int puntosAcumulados) {
		// TODO Auto-generated method stub
		Matriz<Integer> m = new Matriz<Integer>();
		m.inicializarMatriz(puntosAcumulados + 1);
		Vector<Regalo> regalos = new Vector<Regalo>();
		ConjuntoTDA<Regalo> catalogoAux = new Conjunto<Regalo>();
		catalogoAux.inicializarConjunto();
		int cantidad = 0;
		while (!catalogo.conjuntoVacio()) {
			cantidad++;
			Regalo r = catalogo.elegir();
			catalogo.sacar(r);
			catalogoAux.agregar(r);
		}
		catalogo = catalogoAux;
		cantidadRegalos = cantidad;
		regalos.inicializarVector(cantidad + 1);

		// Primer elemento de regalos tiene que ser null
		try {
			regalos.agregarElemento(0, null);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Generar una ordenacion con los regalos del catalogo

		catalogoAux = new Conjunto<Regalo>();
		catalogoAux.inicializarConjunto();
		ColaPrioridad<Regalo> regalosOrdenados = new ColaPrioridad<Regalo>();
		regalosOrdenados.InicializarCola();

		while (!catalogo.conjuntoVacio()) {

			Regalo r = catalogo.elegir();
			regalosOrdenados.AgregarElemento(r, r.obtenerPrecio());
			catalogo.sacar(r);
			catalogoAux.agregar(r);

		}
		catalogo = catalogoAux;
		for (int i = 1; i < regalos.capacidadVector(); i++)
			try {
				Regalo recuperado = regalosOrdenados.RecuperarMinElemento();
				// System.out.println("Regalo recuperado minimo: "+
				// recuperado.obtenerNombre());
				regalos.agregarElemento(i, recuperado);
				regalosOrdenados.EliminarMinPrioridad();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		// Rellena ceros en primera columna matriz
		for (int i = 0; i < regalos.capacidadVector(); i++) {
			m.setearValor(i, 0, 0);
		}

		// Rellena ceros en primera columna matriz
		for (int j = 0; j < m.obtenerDimension(); j++) {
			m.setearValor(0, j, j);
			// System.out.println("Posicion matriz: [" + 0 + "," + j + "]");
			// System.out.println("Vale => [" + j + "]");
		}

		// Rellena matriz
		int val = 0;
		for (int i = 1; i < regalos.capacidadVector(); i++) {
			for (int j = 1; j < m.obtenerDimension(); j++) {
				// System.out.println("Posicion matriz: [" + i + "," + j + "]");
				try {
					boolean loLlevo = this.obtenerMinimo(m, i, j, regalos);
					if (j == m.obtenerDimension() - 1 && loLlevo) {
						maximosRegalosResultado.agregar(regalos.recuperarElemento(i));
						val = val + regalos.recuperarElemento(i).obtenerPuntos();
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return val;
	}

	private ColaPrioridad<Regalo> ordenarRegalos(ConjuntoTDA<Regalo> catalogo) {

		ConjuntoTDA<Regalo> catalogoAux = new Conjunto<Regalo>();
		ColaPrioridad<Regalo> cola = new ColaPrioridad<Regalo>();
		cola.InicializarCola();

		while (!catalogo.conjuntoVacio()) {

			Regalo r = catalogo.elegir();
			cola.AgregarElemento(r, r.obtenerPrecio());
			catalogo.sacar(r);
			catalogoAux.agregar(r);

		}
		catalogo = catalogoAux;
		return cola;

	}

	private Regalo elegirRegaloMenorPrecio(ConjuntoTDA<Regalo> catalogoAux) {
		Regalo regaloMin = new Regalo();
		regaloMin.setearPrecio(0);
		while (!catalogoAux.conjuntoVacio()) {
			Regalo regaloAux = catalogoAux.elegir();
			if (regaloAux.obtenerPrecio() < regaloMin.obtenerPrecio()) {
				regaloMin = regaloAux;
			}
			catalogoAux.sacar(regaloAux);
		}
		return regaloMin;
	}

	private int cantidadRegalos(ConjuntoTDA<Regalo> catalogo) {
		ConjuntoTDA<Regalo> catalogoAux = new Conjunto<Regalo>();
		catalogoAux.inicializarConjunto();
		int cantidad = 0;
		while (!catalogo.conjuntoVacio()) {
			cantidad++;
			Regalo r = catalogo.elegir();
			catalogo.sacar(r);
			catalogoAux.agregar(r);
		}
		catalogo = catalogoAux;
		return cantidad;

	}

	private boolean obtenerMinimo(Matriz<Integer> m, Integer i, Integer j, Vector<Regalo> regalos) throws Exception {
		int minimo;
		int sumaregalos = 0;
		for (int k = 1; k <= i; k++) {
			sumaregalos = sumaregalos + regalos.recuperarElemento(k).obtenerPuntos();
		}
		int p1 = j - sumaregalos;
		int loLlevo;
		if (p1 < 0)
			loLlevo = 99999;
		else
			loLlevo = m.obtenerValor(i - 1, j - sumaregalos);
		int noLoLlevo = m.obtenerValor(i - 1, j);
		if (loLlevo < noLoLlevo)
			minimo = loLlevo;
		else
			minimo = noLoLlevo;

		m.setearValor(i, j, minimo);
		// System.out.println("Vale (else) => [" + minimo + "]");

		return (loLlevo < noLoLlevo);

	}

	private int obtenerPosicionVector(Vector<Integer> puntos, Integer cantPuntos, Matriz<Integer> m) throws Exception {
		int posicion = 0;
		for (int i = 0; i < m.obtenerDimension(); i++) {
			if (puntos.recuperarElemento(i).equals(cantPuntos))
				posicion = i;
		}
		return posicion;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * Interfaces.CanjearPuntosInterface#obtenerTableroMultirubroBalanceado(
	 * TDA.ConjuntoTDA) PROBLEMA 2
	 */

	@Override
	public MatrizTDA<Regalo> obtenerTableroMultirubroBalanceado(ConjuntoTDA<Regalo> regalosARetirar) {

		this.maxDim = (int) Math.sqrt(cantidadRegalos);

		MatrizTDA<Regalo> resultado = new Matriz<Regalo>();
		resultado.inicializarMatriz(maxDim);

		return this.llamadaBacktracking(resultado, 0, 0, regalosARetirar);

	}

	private MatrizTDA<Regalo> llamadaBacktracking(MatrizTDA<Regalo> matriz, int i, int j, ConjuntoTDA<Regalo> regalos) {
		
		System.out.println("");
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		System.out.println("&&&&&&& Llamada backtracking con [" + i + "][" + j + "]");
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");

		

		try {

			if (i == maxDim) {

				if (esSolucion(matriz)) {
					System.out.println("Saliendo por solucion");
					return matriz;
				} else {
					return null;
				}

			} else {

				// Calculo indices siguiente etapa
				int nextI, nextJ;

				if (j < maxDim - 1) {
					nextI = i;
					nextJ = j + 1;
				} else {
					nextI = i + 1;
					nextJ = 0;
				}

				while (!regalos.conjuntoVacio() && nextJ > -1 && nextI > -1) {

					Regalo r = regalos.elegir();
					regalos.sacar(r);
					cantidadRegalos--;

					System.out.println("-----> MATRIZ[" + i + "][" + j + "]: " + r.obtenerNombre());
					matriz.setearValor(i, j, r);

					MatrizTDA<Regalo> m1 = new Matriz<Regalo>();
					m1.inicializarMatriz(maxDim);

					m1 = llamadaBacktracking(matriz, nextI, nextJ, regalos);

					if (m1 != null) {
						return m1;
					} else {
						System.out.println("NO ERA SOLUCION: i=" + nextI + " j=" + nextJ);
						// mostrarTablero(matriz);

						System.out.println("Devolviendo: " + r.obtenerNombre());
						regalos.agregar(r);
						cantidadRegalos++;


						if (nextI == maxDim) {
							// Ultima pos
							nextI--;
							nextJ = maxDim - 1;
						} else {
							if (nextJ > 0)
								nextJ--;
							else {
								nextI--;
								nextJ = 0;
							}
						}

					}

				}
				System.out.println("No quedan mas regalos: i=" + nextI + " j=" + nextJ + " cantidadRegalos=" + cantidadRegalos);
				return null;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Saliendo");
		return null;

	}

	private boolean esSolucion(MatrizTDA<Regalo> matriz) {

		System.out.println("Comprobando SOLUCION");

		if (matriz == null || matriz.obtenerDimension() < this.maxDim)
			return false;
		if (regalosDiferentesOk(matriz)) {
			if (sumasOk(matriz)) {
				if (rubrosOk(matriz)) {
					// System.out.println("MATRIZ VALIDA");
					return true;
				} else {
					// System.out.println("La matriz no verifica RUBROS");
				}
			} else {
				// System.out.println("La matriz no verifica SUMAS");

			}
		} else {
			// System.out.println("La matriz no verifica DIFERENTES");

		}

		return false;
	}

	private boolean rubrosOk(MatrizTDA<Regalo> matriz) {
		// TODO Auto-generated method stub
		return true;
	}

	private boolean rubrosOk2(MatrizTDA<Regalo> matriz) {

		boolean rubrosOk = true;
		for (int i = 0; i < matriz.obtenerDimension(); i++) {
			for (int j = 0; j < matriz.obtenerDimension() - 1; j++) {
				for (int k = 1; k < matriz.obtenerDimension() - 1; k++) {
					if (matriz.obtenerValor(i, j).obtenerNroRubro() == matriz.obtenerValor(i, k).obtenerNroRubro())
						return false;
				}
			}
		}
		// Verifica rubros en columnas
		for (int i = 0; i < matriz.obtenerDimension() - 1; i++) {
			for (int j = 0; j < matriz.obtenerDimension() - 1; j++) {
				int k = 0;
				while (j + k < matriz.obtenerDimension() && i + k < matriz.obtenerDimension()) {
					if (matriz.obtenerValor(i, j).obtenerNroRubro() == matriz.obtenerValor(i + k + 1, j + k + 1).obtenerNroRubro())
						return false;
					k++;
				}
			}
		}
		// Verifica diagonal izquierda
		for (int i = 0; i < matriz.obtenerDimension() - 1; i++) {
			for (int j = 0; j < matriz.obtenerDimension(); j++) {
				int k = 0;
				while (j - k > 0 && i + k < matriz.obtenerDimension()) {
					if (matriz.obtenerValor(i, j).obtenerNroRubro() == matriz.obtenerValor(i + k + 1, j - k - 1).obtenerNroRubro())
						return false;
					k++;
				}
			}
		}
		return rubrosOk;
	}

	// Verifica diagonal derecha
	/*
	 * for (int i = 0; i < matriz.obtenerDimension()-1; i++) for (int j = 0; j <
	 * matriz.obtenerDimension(); j++) { int k=0; while (j-k>0 &&
	 * i+k<matriz.obtenerDimension()){ if (matriz.obtenerValor(i,
	 * j).obtenerNroRubro() == matriz.obtenerValor(i+k+1,
	 * j-k-1).obtenerNroRubro()) return false; k++; } }
	 */
	private boolean sumasOk(MatrizTDA<Regalo> matriz) {

		boolean sumasOk = true;

		int sumaPrimerFila = 0;

		for (int j = 0; j < matriz.obtenerDimension(); j++) {
			sumaPrimerFila = sumaPrimerFila + matriz.obtenerValor(0, j).obtenerPrecio();
		}

		// System.out.println("Suma primera fila: " + sumaPrimerFila);

		for (int i = 1; i < matriz.obtenerDimension(); i++) {
			int sumaFila = 0;
			for (int j = 0; j < matriz.obtenerDimension(); j++) {
				sumaFila = sumaFila + matriz.obtenerValor(i, j).obtenerPrecio();
			}
			// System.out.println("Suma fila: " + sumaFila);
			if (sumaFila != sumaPrimerFila)
				return false;
		}

		int sumaPrimerColumna = 0;
		for (int i = 0; i < matriz.obtenerDimension(); i++) {
			sumaPrimerColumna = sumaPrimerColumna + matriz.obtenerValor(i, 0).obtenerPrecio();
		}

		// System.out.println("Suma primera columna: " + sumaPrimerColumna);

		for (int j = 1; j < matriz.obtenerDimension(); j++) {
			int sumaColumna = 0;
			for (int i = 0; i < matriz.obtenerDimension(); i++) {
				sumaColumna = sumaColumna + matriz.obtenerValor(i, j).obtenerPrecio();
			}
			// System.out.println("Suma columna: " + sumaColumna);

			if (sumaColumna != sumaPrimerColumna)
				return false;
		}

		return sumasOk;
	}

	private boolean regalosDiferentesOk(MatrizTDA<Regalo> matriz) {
		// TODO Auto-generated method stub
		return true;
	}

	private void mostrarTablero(MatrizTDA<Regalo> t) {

		for (int i = 0; i < t.obtenerDimension(); i++) {
			for (int j = 0; j < t.obtenerDimension(); j++) {
				System.out.println("Coordenada x: " + i + " Coordenada y: " + j + " : " + t.obtenerValor(i, j).toString());
			}
		}
	}

}
